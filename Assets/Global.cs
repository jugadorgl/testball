﻿using UnityEngine;


static public class Global 
{
	public	static	int				player_lastattempts	= 0;
	public	static	int				player_attempts		= 0;
	public	static	float			player_current_time	= 0;
	public	static	float			player_best_time	= 0;
	public	static	bool			player_win			= false;
}
