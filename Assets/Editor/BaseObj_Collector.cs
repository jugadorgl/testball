﻿using UnityEditor;
using UnityEngine;
using System.Collections;

public class BaseObj_Collector : MonoBehaviour
{
    [MenuItem("BALL GAME/BaseObj_Collect")]
    static void CollectGame_Obj_For_Init()
    {
		Scene_manager[] _scenes = Resources.FindObjectsOfTypeAll<Scene_manager> () as Scene_manager[];

		if (_scenes.Length > 0)
			for (int i =0; i < _scenes.Length; i++)
				_scenes[i].CollectAllObjects ();
    }

}
