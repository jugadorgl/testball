﻿using UnityEngine;


public class LoadSave_manager : Base_Obj
{
    public static LoadSave_manager insta;
    private string  str_attempts    = "player_attempts";
    private string  str_last_time   = "player_last_time";
    private string  str_best_time   = "player_best_time";


    public override void Init()
    {
        insta = this;
        Load();
    }


    public void Load()
    {
        if (PlayerPrefs.HasKey( str_attempts ) )
        {
            Global.player_attempts = PlayerPrefs.GetInt( str_attempts );
        }
        Global.player_lastattempts = Global.player_attempts;

        if (PlayerPrefs.HasKey( str_last_time ) )
        {
            Global.player_current_time = PlayerPrefs.GetFloat( str_last_time );
        }

        if (PlayerPrefs.HasKey( str_best_time ) )
        {
            Global.player_best_time = PlayerPrefs.GetFloat( str_best_time );
        }
    }


    public void Save()
    {
        PlayerPrefs.SetInt( str_attempts, Global.player_attempts );
        PlayerPrefs.SetFloat( str_last_time, Global.player_current_time );
        PlayerPrefs.SetFloat( str_best_time, Global.player_best_time );
    }

}
