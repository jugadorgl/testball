﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Game_Manager : MonoBehaviour
{
	public	static	Game_Manager	insta;

	[SerializeField]	LoadSave_manager	m_load_save_manager;

	public	Scene_manager			m_MainMenu;
	public	Scene_manager			m_BallGame;
	public	Scene_manager			m_Results;
	private	Scene_manager			m_current_scene = null;


	private void Awake()
	{
		Application.runInBackground		= true;
		insta	= this;
		
		#if UNITY_EDITOR
			PlayerPrefs.DeleteAll();
		#endif
		
		Application.targetFrameRate = 60;
		FirstInit();

		m_MainMenu.Obj.SetActive(false);
		m_BallGame.Obj.SetActive(false);
		m_Results.Obj.SetActive(false);
	}


	private void FirstInit()
    {
		m_load_save_manager.Init();

		m_MainMenu.Init();
		m_BallGame.Init();
		m_Results.Init();
    }


    private void Start()
    {
		SwitchScene( m_MainMenu );
    }


	public void SwitchScene( Scene_manager _new_scene )
    {
		if ( m_current_scene != null)
        {
			m_current_scene.DisableManager();
			m_current_scene.Obj.SetActive(false);
        }

		if (_new_scene != null)
        {
			_new_scene.StartInit();
			_new_scene.Obj.SetActive(true);
			_new_scene.ActivateManager();
			m_current_scene	 = _new_scene;
        }
    }
}
