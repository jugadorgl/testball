﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Scene_manager : Base_Obj
{
	[HideInInspector]
	[SerializeField]private	Base_Obj[] 		m_all_BaseObj;

	
	[ContextMenu("CollectAllObjects")]
	public void CollectAllObjects()
	{
		Base_Obj[] temp_BaseObj			= this.GetComponentsInChildren<Base_Obj> () as Base_Obj[];
		if (temp_BaseObj.Length > 0)
        {
			m_all_BaseObj = new Base_Obj[temp_BaseObj.Length - 1];
			int _counter = 0;
			for ( int i = 0; i < temp_BaseObj.Length; i++)
			{
				if ( temp_BaseObj[i] != this)
                {
					m_all_BaseObj[ _counter ] = temp_BaseObj[i];
					_counter++;
                }
			}
        }
	}

	
	public override void Init()
	{
		base.Init();

		for (int i = 0; i < m_all_BaseObj.Length; i++)
		{
			m_all_BaseObj[i].Init();
		}
	}


	public override void StartInit()
	{
		for (int i = 0; i < m_all_BaseObj.Length; i++)
		{
			m_all_BaseObj[i].StartInit();
		}
	}


	public virtual void ActivateManager()
	{
	}


	public virtual void DisableManager()
	{
	}


	public virtual void Receive( bool value )
    {

    }
}
