﻿using System.Collections;
using UnityEngine;


public class Base_Obj : MonoBehaviour
{
	[HideInInspector]
	public	GameObject		Obj;
	[HideInInspector]
	public	Transform		transf;
	protected	Transform	m_parent;

	protected	bool		m_enabled	= false;
	public		Vector3		m_Offset;


    public virtual void Init()
    {
		Obj			= gameObject;
		transf		= transform;
		m_parent	= transf.parent;
		m_enabled	= false;
    }


	public Transform GetParent()
	{
		return m_parent;
	}


    public virtual void StartInit()
    {
        
    }

	
	public virtual void Play ()
	{

	}


	public virtual void Stop ()
	{

	}

}
