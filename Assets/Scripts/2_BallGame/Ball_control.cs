﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball_control : Base_Obj
{
    private Rigidbody   m_rig;
    public  float       f_speed = 1;

    
    public override void Init()
    {
        base.Init();
        m_rig   = GetComponent<Rigidbody>();
    }


    public override void StartInit()
    {
        m_rig.isKinematic = true;
        m_enabled   = false;
    }

    public void Spawn( Transform _spawnPoint )
    {
        transf.position =  _spawnPoint.position;
        transf.rotation = _spawnPoint.rotation;
        Camera_follower.insta.SetTarget( transf );
    }


    public override void Play()
    {
        m_rig.isKinematic = false;
        m_enabled = true;
    }

    
    public override void Stop()
    {
        m_enabled = false;
        m_rig.isKinematic = true;
    }


    private void FixedUpdate()
    {
        if (m_enabled)
        {
            if (Input.GetKey(KeyCode.S) )
            {
                m_rig.AddTorque( Vector3.forward * f_speed, ForceMode.Acceleration );
            }
            else if (Input.GetKey(KeyCode.W) )
            {
                m_rig.AddTorque( Vector3.back * f_speed, ForceMode.Acceleration );
            }

            if (Input.GetKey(KeyCode.A) )
            {
                m_rig.AddTorque( Vector3.right * f_speed, ForceMode.Acceleration );
            }
            else if (Input.GetKey(KeyCode.D) )
            {
                m_rig.AddTorque( Vector3.left * f_speed, ForceMode.Acceleration );
            }
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Respawn"))
        {
            Stop();
            Game_Manager.insta.m_BallGame.Receive( false );
        }

        if (collision.collider.CompareTag("Finish"))
        {
            Stop();
            Game_Manager.insta.m_BallGame.Receive( true );
        }
    }

}
