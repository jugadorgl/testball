﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGame_manager : Scene_manager
{
    [SerializeField] private Transform      m_start_point;
    [SerializeField] private Ball_control   m_Ball_player;
    private float    f_start_time;


    public override void StartInit()
    {
        base.StartInit();
    }

    
    public override void ActivateManager()
    {
        m_Ball_player.Spawn( m_start_point );
        StartCoroutine( IDelayedStart() );
    }


    IEnumerator IDelayedStart()
    {
        yield return new WaitForSeconds(0.25f);
        m_Ball_player.Play();
        f_start_time = Time.time;
    }


    public override void Receive( bool value )
    {
        Global.player_win = value;
        CalcResults();
        StartCoroutine( IDelayedEnd() );
    }

    
    private void CalcResults()
    {
        Global.player_current_time =  (float)(Mathf.Round( (Time.time - f_start_time) * 100 ) ) / 100f;

        if ( Global.player_win )
        {
            if ( ( Global.player_best_time == 0 ) ||
                 ( ( Global.player_best_time > 0 ) && ( Global.player_current_time < Global.player_best_time )) )
            {
                Global.player_best_time = Global.player_current_time;
            }
        }
  
        Global.player_attempts ++;
        
        LoadSave_manager.insta.Save();
    }


    IEnumerator IDelayedEnd()
    {
        yield return new WaitForSeconds(0.5f);
        Game_Manager.insta.SwitchScene( Game_Manager.insta.m_Results );
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere( m_start_point.position, 0.1f );
    }
}
