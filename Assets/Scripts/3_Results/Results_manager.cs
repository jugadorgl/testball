﻿using UnityEngine;


public class Results_manager : Scene_manager
{
    [SerializeField]private Result_panel    m_panel_Result;

    public override void StartInit()
    {
        base.StartInit();
        Camera_follower.insta.SetTarget( null );
        m_panel_Result.Play();
    }
}
