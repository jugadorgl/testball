﻿using UnityEngine;
using System.Collections;

public class FadeScript : Base_Obj 
{
	public	SpriteRenderer		m_render;

	private	Color				m_Color;
	private	float 				f_default_z_pos;

	[HideInInspector]
	public	float				t;
	public	bool				m_active	= false;

	private	MonoBehaviour		m_Receiver;
	private	string				m_Func;

	private CTimer				t_Timer	= new CTimer();
	private float				f_alpha;
	private float				f_targetAlpha;


	public void	AddCBFunc( MonoBehaviour _receiver, string _func )
	{
		m_Receiver	= _receiver;
		m_Func 		= _func;
	}


	private void RunCB()
	{
		if (m_Receiver != null) 
		{
			m_Receiver.SendMessage (m_Func, SendMessageOptions.RequireReceiver);
		}
		if (m_Color.a <= 0.05f) 
		{
			m_render.enabled	= false;
		}
	}



	public void ClearCB()
	{
		m_Receiver = null;
	}


	public void SetDefaultZ()
	{
		transform.position = new Vector3 (transform.position.x, transform.position.y, f_default_z_pos);
	}

	
	public void SetLayerTop( bool value )
	{
		m_render.sortingLayerID	= value ? 1 : 0;
	}


	public void InitFader ( float _alpha ) 
	{
		m_Color			= m_render.color;
		m_Color.a		= _alpha;
		m_render.color	= m_Color;

		if (m_Color.a <= 0.05f) 
		{
			m_render.enabled	= false;
		}
		f_default_z_pos	= transform.position.z;
		m_Receiver		= null;
	}


	public void StartFade ( float _targetAlpha, float _time)
	{
		m_render.enabled	= true;
		f_alpha				= m_Color.a;
		f_targetAlpha		= _targetAlpha;
		gameObject.SetActive(true);

		if (t_Timer == null)
		{
			t_Timer	= new CTimer();
		}
		t_Timer.Start(_time);
	}


	void Update()
	{
		if (t_Timer.active)
		{
			if (t_Timer.Update(Time.deltaTime))
			{
				m_Color.a			= f_targetAlpha;
				m_render.color		= m_Color;
				if (m_Color.a <= 0.05f) 
				{
					m_render.enabled	= false;
				}
				RunCB();
			}
			else
			{
				m_Color.a		= Mathf.Lerp(f_alpha, f_targetAlpha, t_Timer.w);
				m_render.color	= m_Color;
			}
		}
	}

}
