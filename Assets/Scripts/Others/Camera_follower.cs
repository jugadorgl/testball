﻿using UnityEngine;


public class Camera_follower : Base_Obj
{
    public  static   Camera_follower insta;
    private Transform   m_target;
    public  float       f_target_distance       = 3;
    public  float       f_target_distance_dump  = 0.25f;
    
    private Vector3     tPos;
    private Vector3     v_velocity = Vector3.zero;



    private void Awake()
    {
        insta = this;
        base.Init();
    }


    public void SetTarget( Transform _target )
    {
        m_target = _target;
        if ( m_target == null )
        {
            transf.position = new Vector3(0,1,-10);
            transf.rotation = Quaternion.identity;
            m_enabled   = false;
        }
        else
        {
            CalculateDistance();
            Setposition( 0f );
            m_enabled   = true;
        }
    }



    private void LateUpdate()
    {
        if (m_enabled)
        {
            CalculateDistance();
        }
    }


    private void CalculateDistance()
    {
        tPos  = m_target.position - m_Offset * f_target_distance;
        Setposition( f_target_distance_dump );
        SetRotation();
    }


    
    private void Setposition( float _dump )
    {
        transf.position = Vector3.SmoothDamp(transf.position, tPos, ref v_velocity, _dump);
    }


    private void SetRotation()
    {
        transf.LookAt( m_target );
    }
}
