﻿using UnityEngine;

public class MainMenu_manager : Scene_manager
{
    [SerializeField]private GameObject      m_btn_Play;
    [SerializeField]private GameObject      m_btn_Result;

    public override void StartInit()
    {
        base.StartInit();
        Camera_follower.insta.SetTarget( null );
    }


    public void Btn_Play()
    {
        Game_Manager.insta.SwitchScene( Game_Manager.insta.m_BallGame );
    }


    public void Btn_Result()
    {
        Game_Manager.insta.SwitchScene( Game_Manager.insta.m_Results );
    }

}
