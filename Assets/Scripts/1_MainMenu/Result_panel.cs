﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class Result_panel : Base_Obj
{
    public  TextMeshProUGUI     m_win;
    public  TextMeshProUGUI     m_attempts;
    public  TextMeshProUGUI     m_last_time;
    public  TextMeshProUGUI     m_best_time;


    public override void StartInit()
    {
        base.StartInit();
        Obj.SetActive(false);
    }


    public override void Play()
    {
        if ( Global.player_attempts != Global.player_lastattempts )
        {
            m_win.text          = Global.player_win ? "Вы выиграли" : "Вы проиграли";
            m_win.color         = Global.player_win ? Color.green : Color.red;
        }
        else
        {
            m_win.text = "";
        }
        m_attempts.text     = Global.player_attempts.ToString();
        m_last_time.text    = Global.player_current_time.ToString();
        m_best_time.text    = Global.player_best_time.ToString();
        Obj.SetActive(true);
    }


    public override void Stop()
    {
        Obj.SetActive(false);
    }


    public void Btn_Back()
    {
        Game_Manager.insta.SwitchScene( Game_Manager.insta.m_MainMenu );
    }
}
